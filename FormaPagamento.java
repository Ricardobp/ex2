package Ex2;

   public enum FormaPagamento{
        CARTAODECREDITO("Cartao de Credito"), 
        CARTAODEDEBITO("Cartao de Dedito"),
        DINHEIRO("Dinheiro"),
        CHEQUE("Cheque");
        
        private String forma;
        
        FormaPagamento(String forma){
            this.forma = forma;
        }
        
        public String getForma(){
            return forma;
        }
    }


