package Ex2;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class Tela{
    Planilha planilha = new Planilha();

     public void executar(int opcao) throws ParseException {
        switch(opcao){
            case 1:
                addPlanilha();
                break;
            case 2:
                toString();
                break;
            case 3:
                break;
            default:
                System.out.println("% Escolha uma opcao valida %");
        }
    }


    public void addPlanilha() throws ParseException {

         Scanner sc = new Scanner(System.in);

            System.out.println("Padrão -> dd/MM/yyyy HH:mm:ss");
            System.out.println();
            System.out.print("Data e Hora de inicio: ");
            planilha.dataInicio = sc.nextLine();
            DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            Date dt = df.parse(planilha.dataInicio);
            System.out.println(dt);

            System.out.println();
            System.out.print("Descrição: ");
            planilha.descricao = sc.nextLine();

            System.out.print("Local: ");
            planilha.local = sc.nextLine();

            System.out.print("Valor: ");
            planilha.valor = sc.nextDouble();

            System.out.print("Forma de pagamento: ");
            System.out.print("Forma de pagamento: ");

    }

    public void mostrarTela() throws ParseException {
        Scanner sc = new Scanner(System.in);
        int opcao = 0;
        do{
            System.out.println();
            System.out.println("Gestão da Empresa");
            System.out.println("------------------------");
            System.out.println("1. Adicionar Planilha");
            System.out.println("2. Listar Itens");
            System.out.println("3. Sair");
            System.out.println("------------------------");
            System.out.print("Opcao: ");
            opcao = sc.nextInt();
            executar( opcao );
        }while(opcao != 3 );
    }


    public static void main(String[] args) throws ParseException {
        Tela tela = new Tela();
        tela.mostrarTela();
    }

    @Override
    public String toString() {
        return "Tela{" +
                "planilha=" + planilha +
                '}';
    }
}
